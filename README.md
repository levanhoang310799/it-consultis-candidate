## Installation
Install project:
```
docker compose build
docker compose up -d
docker exec -it it_consultis_drupal bash
composer install
composer update
zcat database/db.sql.gz | drush sqlc
```

## Admin site
After creating your project, do the following:
* Admin url: http://localhost:80/en/about-us.
* Login url: http://localhost:80/user
* Account: admin/admin

## Frontend Tools
We are using webpack to build FE. Using node vesion 12
Check your node vesion
```
node --version
```
Install
```
npm install
```

Build FE
```
cd web/themes/custom/it_consultis
npm run watch
```

