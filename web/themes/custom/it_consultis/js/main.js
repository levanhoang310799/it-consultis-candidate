/**
 * @file
 * Javascript functionality for it consultis custom theme.
 */

(function ($, Drupal, once) {

  'use strict';

  Drupal.itConsultisTheme = Drupal.itConsultisTheme || {};
  Drupal.behaviors.itConsultisThemeBehavior = {
    attach: function(context) {
      Drupal.itConsultisTheme.carousel(context);
      Drupal.itConsultisTheme.pillars(context);
    }
  };

  Drupal.itConsultisTheme.carousel = function(context) {
    const $node_items = once('carouseItems', '.node--type-carousel', context);
    if ($node_items.length > 0) {
      $node_items.forEach(function($node_item) {
        var $parent_item = $($node_item).find('.field--name-field-carousel-items');
        $parent_item.slick({
          infinite: true,
          speed: 300,
          slidesToShow: 1,
          slidesToScroll: 1,
          prevArrow: '.node-carousel-alter-button.slick-prev',
          nextArrow: '.node-carousel-alter-button.slick-next'
        });
      });
    }
  };

  Drupal.itConsultisTheme.pillars = function (context) {
    const $node_items = once('pillarsItems', '.node--type-pillars', context);
    if ($node_items.length > 0) {
      $node_items.forEach(function ($node_item) {
        const $node_content_wrapper = $($node_item).find('.field--name-field-pillar-items .node-content--wrapper');
        if ($node_content_wrapper.length > 0) {
          $node_content_wrapper.matchHeight();
        }
        const $pillars = $($node_item).find('.field--name-field-pillar-items > .field__item');
        if ($pillars.length > 0) {
          $pillars.matchHeight();
        }
      })
    }
  }

})(jQuery, Drupal, once);
