# Use the official PHP image with Apache as the base image
FROM php:8.1.0-apache

# Install system dependencies
RUN apt-get update && \
    apt-get install -y git default-mysql-client libpng-dev libjpeg-dev libfreetype6-dev zip unzip vim && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*

# Thêm đường dẫn đến thư mục composer vào biến PATH
RUN echo 'export PATH="$PATH:/var/www/html/vendor/bin"' >> ~/.bashrc


# Install PHP extensions required by Drupal
RUN docker-php-ext-configure gd --with-freetype --with-jpeg && \
    docker-php-ext-install gd pdo pdo_mysql opcache

# Install Composer
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

# Install Node.js and Yarn
RUN curl -fsSL https://deb.nodesource.com/setup_14.x | bash - && \
    apt-get install -y nodejs && \
    npm install -g yarn

# Install Gulp globally
RUN npm install -g gulp-cli

RUN git config --global http.sslVerify false

RUN git config --global --add safe.directory /var/www/html

# Enable Apache modules
RUN a2enmod rewrite

# Set the working directory
WORKDIR /var/www/html

# Update the Apache default virtual host configuration
RUN sed -i 's:/var/www/html:/var/www/html/web:g' /etc/apache2/sites-available/000-default.conf

# Copy Drupal files into the web root
COPY . /var/www/html/

# Set proper permissions for Drupal files
RUN useradd -ms /bin/bash admin
RUN chown -R admin:admin /var/www/html/
RUN chmod -R 775 /var/www/html/web/sites/
USER admin

RUN echo 'alias drush="/var/www/html/vendor/drush/drush/drush"' >> ~/.bashrc


# Expose port 80 to the outside world
EXPOSE 80

# Start the Apache web server
CMD ["apache2-foreground"]
